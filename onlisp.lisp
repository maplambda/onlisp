;; defining functions
(defun double (x) (* x 2))
(double 1)

;; ch2
;; functions

;; Functions are objects in their own right. What defun really does is build one, and store it under the name given as the first argument. So as well as calling double, we can get hold of the function which implements it. The usual way to do so is by using the #’ (sharp-quote) operator. 

#'double
;;#<Compiled-function DOUBLE #x3020010C5D6F>

;; a Common Lisp function is a first-class object, with all the same rights as more familiar objects like numbers and strings. So we can pass this function as an argument, return it, store it in a data structure, and so on:

(eq #'double (car (list #'double)))
;; T

;; literal function (lambda)
(lambda (x) (* x 2))
;; #<Anonymous Function #x3020012DC38F>

;; Beneath the surface, defun is setting the symbol-function of its first argu- ment to a function constructed from the remaining arguments. The following two expressions do approximately the same thing:

(defun double (x) (* x 2))

(setf (symbol-function 'double)
      #'(lambda (x) (* x 2)))

;; clousures

(let ((counter 0))
  (defun new-id ()
    (incf counter))
  (defun reset-id ()
    (setq counter 0)))


;; takes a number and returns a closure which,
;; when called, add that number to it's argument
(defun make-adder (n)
  #'(lambda (x) (+ x n)))

(funcall (make-adder 10) 10)

(setq add2 (make-adder 2)
      add10 (make-adder 10))

(funcall add2 5)
(funcall add10 3)

(defun make-adderb (n)
  #'(lambda (x &optional change)
      (if change
	  (setq n x)
	  (+ x n))))

;; apply some function to all the elements of a list
(mapcar #'(lambda (x) (+ 2 x)) '(2 5 7 3))

;; ch3
;; functional programming

;; multiple value return
(defun powers(x)
  (values x (sqrt x) (expt x 2)))

(multiple-value-bind (base root square) (powers 4)
  (list base root square))

;; ch4
;; utility functions

;; operations on lists

(defun last1 (lst)
  (car (last lst)))

(defun single (lst)
  (and (consp lst) (not (cdr lst))))

(defun append1 (lst obj)
  (append lst (list obj)))

(defun conc1 (lst obj)
  (nconc lst (list obj)))

(defun mklist (obj)
  (if (listp obj)
      obj
      (list obj)))

(defun longer (x y)
  (labels ((compare (x y)
	     (and (consp x)
		  (or (null y)
		      (compare (cdr x) (cdr y))))))
    (if (and (listp x) (listp y))
	(compare x y)
	(> (length x) (length y)))))

(longer '(a b c) '(a b))
(longer '(a b) '(a b c))

(defun filter (fn lst)
  (let ((acc nil))
    (dolist (x lst)
      (let ((val (funcall fn x)))
	(if val (push val acc))))
    (nreverse acc)))

(filter #'(lambda (x)
	    (if (numberp x)
		(1+ x)))
	'(a 1 2 b 3 c d 4))

(defun group (source n)
  (if (zerop n)
      (error "zero length"))
  (labels ((rec (source acc)
	     (let ((rest (nthcdr n source)))
	       (if (consp rest)
		   (rec rest (cons (subseq source 0 n) acc))
		   (nreverse (cons source acc))))))
    (if source (rec source nil) nil)))

(group '(a b c d e f g) 2)

;; doubly-recursive list utilities

(defun flatten (x)
  (labels ((rec (x acc)
	     (cond ((null x) acc)
		   ((atom x) (cons x acc))
		   (t (rec (car x) (rec (cdr x) acc))))))
    (rec x nil)))

(flatten '(a (b c) ((d e) f)))

(defun prune (test tree)
  (labels ((rec (tree acc)
	     (cond ((null tree) (nreverse acc))
		   ((consp (car tree))
		    (rec (cdr tree)
			 (cons (rec (car tree) nil) acc)))
		   (t (rec (cdr tree)
			   (if (funcall test (car tree))
			       acc
			       (cons (car tree) acc)))))))
    (rec tree nil)))

(prune #'evenp '(1 2 (3 (4 5) 6) 7 8 (9)))

;; mapping functions


;; -> works for sequences of objects of any kind. the sequence begins with
;; the object given as the second argument, the end of the sequence is defined
;; by the function given as the third argument, and successors are generated by
;; the function given as the fourth argument.
(defun map-> (fn start test-fn succ-fn)
  (do ((i start (funcall succ-fn i))
       (result nil))
      ((funcall test-fn i) (nreverse result))
    (push (funcall fn i) result)))

(map-> #'(lambda (x) (1+ x))
       0
       #'(lambda (x) (>= x 10))
       #'(lambda (x) (+ x 1)))

(defun mapa-b (fn a b &optional (step 1))
  (map-> fn
	 a
	 #'(lambda (x) (>= x b))
	 #'(lambda (x) (+ x step))))

(mapa-b #'(lambda (x) (1+ x))
	0
	10
	1)

(defun map0-n (fn n)
  (mapa-b fn 0 n))

(map0-n #'(lambda (x) (1+ x)) 10)

(defun map1-n (fn n)
  (mapa-b fn 1 n))

(map1-n #'(lambda (x) (1+ x)) 10)

;; ch5
;; returning functions

(defun joiner (obj)
  (typecase obj
    (cons #'append)
    (number #'+)))

(apply (joiner 1) '(2 3))

(defun join (&rest args)
  (apply (joiner (car args)) args))

(join '(4 5 6) '(7 8 9))
(join 1 2)
(join '((1 2)) '((3 4)))

;; compliment built in
;; Before complement, Common Lisp had pairs of functions like remove-if and remove-if-not, subst-if and subst-if-not, and so on.
;; With complement we can do without half of them.
;;(defun complement (fn)
;;  #'(lambda (&rest args) (not (apply fn args))))

(complement #'oddp)

;; We may not be able to make Common Lisp smaller, but we can do something almost as good: use a smaller subset of it.
;; Can we define any new operators which would, like complement and setf, help us toward this goal? There is at least one
;; other way in which functions are grouped in pairs. Many functions also come in a destructive version: remove-if and delete-if,
;; reverse and nreverse, append and nconc. By defining an operator to return the destructive counterpart of a function, we would
;; not have to refer to the destructive functions directly.

(defvar *!equivs* (make-hash-table))

(defun ! (fn)
  (or (gethash fn *!equivs*) fn))

(defun def! (fn fn!)
  (setf (gethash fn *!equivs*) fn!))

(def! #'remove-if #'delete-if)

(funcall (! #'remove-if) #'oddp '(1 2 3 4 5 6 7 8 9 10))

;; memoizing

(defun memoize (fn)
  (let ((cache (make-hash-table :test #'equal)))
    #'(lambda (&rest args)
	(multiple-value-bind (val win) (gethash args cache)
	  (if win
	      val
	      (setf (gethash args cache)
		    (apply fn args)))))))

(setq slowid (memoize #'(lambda (x) (sleep 5) x)))
(time (funcall slowid 1))

(time (funcall slowid 1))


;; composing functions

(defun compose (&rest fns)
  (if fns
      (let ((fn1 (car (last fns)))
	    (fns (butlast fns)))
	#'(lambda (&rest args)
	    (reduce #'funcall fns
		    :from-end t
		    :initial-value (apply fn1 args))))
      #'identity))

(funcall (compose #'list #'1+) 10)
;; (11)
(funcall (compose #'1+ #'find-if) #'oddp '(2 3 4))
;; 4

(defun our-length (lst)
  (if (null lst)
      0
      (1+ (our-length (cdr lst)))))

(our-length '(1 2 3))

(defun our-every (fn lst)
  (if (null lst)
      t
      (and (funcall fn (car lst))
	   (our-every fn (cdr lst)))))

(our-every #'evenp '(1 2 3 4 5 6))
(our-every #'evenp '(2 4 6 8 10))

;;function-builder called lrec (“list recurser”) which should be able to generate most functions that recurse on successive cdrs of a list.

(defun lrec (rec &optional base)
  (labels ((self (lst)
	     (if (null lst)
		 (if (functionp base)
		     (funcall base)
		     base)
		 (funcall rec (car lst)
			  #'(lambda ()
			      (self (cdr lst)))))))
    #'self))

;; length using lrec
(funcall (lrec #'(lambda (x f) (1+ (funcall f))) 0) '(1 2 3))

;; every-odd?
(funcall (lrec #'(lambda (x f) (and (oddp x) (funcall f))) t) '(1 3 5))

;; copy-list
(funcall (lrec #'(lambda (x f) (cons x (funcall f)))) '(1 2 3))

;; remove-duplicates using lrec
(funcall (lrec #'(lambda (x f) (adjoin x (funcall f)))) '(1 2 2 3))

(defun our-copy-tree (tree)
  (if (atom tree)
      tree
      (cons (our-copy-tree (car tree))
	    (if (cdr tree) (our-copy-tree (cdr tree))))))

(our-copy-tree '((a b (c d)) (e) f))
;; ((A B (C D)) (E) F)

(defun count-leaves (tree)
  (if (atom tree)
      1
      (+ (count-leaves (car tree))
	  (or (if (cdr tree)
		  (count-leaves (cdr tree)))
	      1))))

(count-leaves '((a b (c d)) (e) f))
; 10

(defun flatten (tree)
  (if (atom tree)
      (mklist tree)
      (nconc (flatten (car tree))
	     (if (cdr tree) (flatten (cdr tree))))))

(flatten '((a b (c d)) (e) f ()))

;; function intersection
(defun fint (fn &rest fns)
  (if (null fns)
      fn
      (let ((chain (apply #'fint fns)))
	#'(lambda (x)
	    (and (funcall fn x) (funcall chain x))))))

(funcall (fint #'numberp #'oddp #'(lambda (x) (eq x 13))) 13)
;; T
(funcall (fint #'numberp #'oddp #'(lambda (x) (eq x 13))) 12)
;; NIL

(defun rfind-if (fn tree)
  (if (atom tree)
      (and (funcall fn tree) tree)
      (or (rfind-if fn (car tree))
	  (if (cdr tree) (rfind-if fn (cdr tree))))))

(rfind-if (fint #'numberp #'oddp) '(2 (3 4) 5))

; the function fun is like fint but uses or instead of and
(defun fun (fn &rest fns)
  (if (null fns)
      fn
      (let ((chain (apply #'fun fns)))
	#'(lambda (x)
	    (or (funcall fn x) (funcall chain x))))))

(funcall (fun #'numberp #'oddp #'(lambda (x) (eq x 13))) 12)
;T					;
(funcall (fun #'oddp #'(lambda (x) (eq x 13))) 12)
; NIL

;; tree traverse

(defun ttrav (rec &optional (base #'identity))
  (labels ((self (tree)
	     (if (atom tree)
		 (if (functionp base)
		     (funcall base tree)
		     base)
		 (funcall rec (self (car tree))
			  (if (cdr tree)
			      (self (cdr tree)))))))
    #'self))

;; our-copy-tree
(ttrav #'cons)

;; count-leaves
(ttrav #'(lambda (l r) (+ l (or r 1)) 1))

(funcall (ttrav #'(lambda (l r) (+ l (or r 1))) 1) '((a b (c d)) (e) f))
;; 10

;; flatten
(ttrav #'nconc #'mklist)

;; general tree recurse
(defun trec (rec &optional (base #'identity))
  (labels
      ((self (tree)
	 (if (atom tree)
	     (if (functionp base)
		 (funcall base tree)
		 base)
	     (funcall rec tree
		      #'(lambda ()
			  (self (car tree)))
		      #'(lambda ()
			  (if (cdr tree)
			      (self (cdr tree))))))))
    #'self))

;; flatten
(funcall (trec #'(lambda (o l r) (nconc (funcall l) (funcall r)))
	       #'mklist)
	 '(a (b c) ((d e) f)))

;; rfind-if
(funcall (trec #'(lambda (o l r) (or (funcall l) (funcall r)))
	       #'(lambda (tree) (and (oddp tree) tree)))
	 '(2 (3 4) 5))
;; 3

;; ch7
;; macros

(defmacro nil! (var)
  `(setq ,var nil))

(setq *test* 'test)
*test*
(nil! *test*)
*test*

(pprint (macroexpand-1 '(nil! *test*)))

(defmacro mac (expr)
  `(pprint (macroexpand-1 ,expr)))


;; simplify member

(member 'b '(a b c d) :test #'eq)

(defmacro member-eq (obj lst)
  `(member ,obj ,lst :test #'eq))

(member-eq 'b '(a b c d))

;; ch8
;; when to use macros

(defun avg (&rest args)
  (/ (apply #'+ args) (length args)))

;; At compile-time we may not know the values of the arguments, but we do know how many there are,
;; so the call to length could just as well be made then
(defmacro avg-m (&rest args)
  `(/ (+ ,@args) ,(length args)))
;; (/ (+ 1 2 3) 3)

;; ch9
;; Avoiding Capture

;; We need some way to ensure that a symbol is unique. The Common Lisp function gensym exists just for this purpose.
;; It returns a symbol, called a gensym, which is guaranteed not to be eq to any symbol either typed in or constructed by
;; a program.
(gensym)
;; #:G69383

;; ch11
;; classic macros

(let ((x 'b))
  (list x))
;; (b)

(defmacro our-let (binds &body body)
  `((lambda ,(mapcar #'(lambda (x)
			 (if (consp x)
			     (car x)
			     x))
		     binds)
      ,@body)
    ,@(mapcar #'(lambda (x)
		  (if (consp x)
		      (cadr x)
		      nil))
	      binds)))

(our-let ((x 'b))
  (list x))
;; (B)

(mac '(our-let ((x 'b))
       (list x)))

;; ((LAMBDA (X) (LIST X)) 'B)

(mac '(our-let ((x 1) (y 2))
	  (+ x y)))

;; ((LAMBDA (X Y) (+ X Y)) 1 2)


;; ch14
;; anaphoric macros

;; When you use an aif, the symbol it is left bound to the result returned by the test clause. In the macro call, it seems to be free, but in fact the expression (foo it) will be inserted by expansion of the aif into a context in which the symbol it is bound
(defmacro aif (test-form then-form &optional else-form)
  `(let ((it ,test-form))
     (if it ,then-form ,else-form)))

(defun big-long-calculation ()
  'finished)

(aif (big-long-calculation)
     (print it))

;; recursive function with local function

(defun count-instances (obj lists)
  (labels ((instances-in (list)
	     (if list
		 (+ (if (eq (car list) obj) 1 0)
		    (instances-in (cdr list)))
		 0)))
    (mapcar #'instances-in lists)))

(count-instances 'a '((a b c) (d a r p a) (d a r) (a a)))
;; (1 2 1 2)

(defmacro alambda (parms &body body)
  `(labels ((self ,parms ,@body))
     #'self))

;; factorial using alambda
(funcall (alambda (x)
  (if (= x 0) 1
      (* x (self (1- x))))) 10)
;; 3628800

(defun acount-instances (obj lists)
  (mapcar (alambda (list)
	    (if list
		(+ (if (eq (car list) obj) 1 0)
		   (self (cdr list)))
		0))
	  lists))

(acount-instances 'a '((a b c) (d a r p a) (d a r) (a a)))
;; (1 2 1 2)

(setf edible (make-hash-table)
      (gethash 'olive-oil edible) t
      (gethash 'motor-oil edible) nil)

;; gethash always returns two values, the second indicating whether anything was found
(gethash 'motor-oil edible)

;; if you want to detect all three possible cases, you can use an idiom like the following:
(defun edible? (x)
  (multiple-value-bind (val found?) (gethash x edible)
    (if found?
	(if val 'yes 'no)
	'maybe)))

(mapcar #'edible? '(motor-oil olive-oil iguana))
;; (NO YES MAYBE)

(defmacro aif2 (test &optional then else)
  (let ((win (gensym)))
    `(multiple-value-bind (it ,win) ,test
       (if (or it ,win) ,then ,else))))

(defun aedible? (x)
  (aif2 (gethash x edible)
	(if it 'yes 'no)
	'maybe))

(mapcar #'aedible? '(motor-oil olive-oil iguana))
;; (NO YES MAYBE)

;; ch16
;; macro-defining macros

(defmacro abbrev (short long)
  `(defmacro ,short (&rest args)
     `(,',long ,@args)))

(mac '(abbrev mvbind multiple-value-bind))

;; (DEFMACRO MVBIND (&REST ARGS) (LIST* 'MULTIPLE-VALUE-BIND ARGS))

;; Anaphoric macros
;; we can define a+ so that as with aand, it is always bound to the value returned by the previous argument

(defmacro a+ (&rest args)
  (a+expand args nil))

(defun a+expand (args syms)
  (if args
      (let ((sym (gensym)))
	    `(let* ((,sym ,(car args))
		    (it ,sym))
	       ,(a+expand (cdr args)
			  (append syms (list sym)))))
      `(+ ,@syms)))

(defmacro alist (&rest args)
  (alist-expand args nil))

(defun alist-expand (args syms)
  (if args
      (let ((sym (gensym)))
	    `(let* ((,sym ,(car args))
		    (it ,sym))
	       ,(alist-expand (cdr args)
			      (append syms (list sym)))))
      `(list ,@syms)))

(defun mass-cost (menu-price)
  (a+ menu-price (* it .05) (* it 3)))

;; The Massachusetts Meals Tax is 5%, and residents often calculate the tip by tripling the tax. By this formula,
;; the total cost of the broiled scrod at Dolphin Seafood is therefore:
(mass-cost 7.95)
;; 9.54

(mac '(a+ menu-price (* it .05) (* it 3)))

;;(LET* ((#:G69850 MENU-PRICE) (IT #:G69850))
;;  (LET* ((#:G69851 (* IT 0.05)) (IT #:G69851))
;;    (LET* ((#:G69852 (* IT 3)) (IT #:G69852))
;;      (+ #:G69850 #:G69851 #:G69852))))

;;ch25.

;; Object-oriented programming, at a minimum, implies
;; 1. objects which have properties
;; 2. and respond to messages,
;; 3. and which inherit properties and methods from their parents.

(defmethod print-object ((x hash-table) stream)
  (format stream "#<~A>" (gethash 'name x)))

(defparameter *obj* (make-hash-table))
(gethash 'color *obj*)

(setf (gethash 'name *obj*) '*obj*)
(setf (gethash 'color *obj*) #'(lambda (obj param)
				 (format nil "color of ~a is ~a" obj param)))

(funcall (gethash 'color *obj*) *obj* 'blue)

(defun tell (obj message &rest args)
  (apply (gethash message obj) obj args))

(tell *obj* 'color 'red)

(defparameter *obj2* (make-hash-table))
(setf (gethash 'name *obj2*) '*obj2*)
(setf (gethash 'color *obj2*) #'(lambda (obj param)
				 (format nil "parent color of ~a is ~a" obj param)))
(setf (gethash 'parent *obj*) *obj2*)

(remhash 'color *obj*)

(defun rget (obj prop)
  (multiple-value-bind (val win) (gethash prop obj)
      (if win
	  (values val win)
	  (let ((par (gethash 'parent obj)))
	    (and par (rget par prop))))))


(rget *obj* 'color)

(defun tell (obj message &rest args)
  (apply (rget obj message) obj args))

(tell *obj* 'color 'red)


(defun rget (obj prop)
  (some2 #'(lambda (a) (gethash prop a))
	 (get-ancestors obj)))

(defun get-ancestors (obj)
  (labels ((getall (x)
	     (append (list x)
		     (mapcan #'getall
			     (gethash 'parents x)))))
    (stable-sort (delete-duplicates (getall obj))
		 #'(lambda (x y)
		     (member y (gethash 'parents x))))))

;; The utility some2 is a version of some for use with functions like gethash that indicate success or failure in the second return value

(defun some2 (fn lst)
  (if (atom lst)
      nil
      (multiple-value-bind (val win) (funcall fn (car lst))
	(if (or val win)
	    (values val win)
	    (some2 fn (cdr lst))))))

(defvar *table* (make-hash-table))

(setf (gethash 't *table*) 'test)
(some2 #'(lambda (x) (gethash x *table*)) '(t u))
;; TEST, T

(some2 #'(lambda (x) (gethash x *table*)) '(u t))
;; TEST, T

(some2 #'(lambda (x) (gethash x *table*)) '(u))
;; NIL

;; adding multiple inheritance

(defun obj (&rest parents)
  (let ((obj (make-hash-table)))
    (setf (gethash 'parents obj) parents)
    (ancestors obj)
    obj))

(defun ancestors (obj)
  (or (gethash 'ancestors obj)
      (setf (gethash 'ancestors obj) (get-ancestors obj))))

(defun rget (obj prop)
  (some2 #'(lambda (a) (gethash prop a))
	 (ancestors obj)))

(defparameter *obj3* (obj *obj* *obj2*))
(tell *obj3* 'color 'red)

(defmacro defprop (name &optional meth?)
  `(progn
     (defun ,name (obj &rest args)
       ,(if meth?
	    `(run-methods obj ',name args)
	    `(rget obj ',name)))
     (defsetf ,name (obj) (val)
       `(setf (gethash ',',name ,obj) ,val))))

(defun run-methods (obj name args)
  (let ((meth (rget obj name)))
    (if meth
	(apply meth obj args)
	(error "No ~A method for ~A." name obj))))

(setq myrec (obj))
(defprop height)
(defprop width)
(defprop area t)
(setf (height myrec) 4
      (width myrec) 3
      (area myrec) #'(lambda (r)
		(* (height r) (width r))))

(gethash 'height myrec)
(rget myrec 'height)

(height myrec)
(width myrec)
(area myrec)

(defmacro defmeth ((name) obj parms &body body)
  `(progn
     (defprop ,name t)
     (setf (,name ,obj) #'(lambda ,parms ,@body))))

(mac '(defmeth (area) myrec (r)
       (* (height r) (width r))))

(defmeth (area) myrec (r)
  (* (height r) (width r)))

(area myrec)

;; clos

(defclass random-dot ()
  ((x :accessor dot-x :initform (random 100))
   (y :accessor dot-y :initform (random 100))))

(mapcar #'(lambda (name)
	    (let ((rd (make-instance 'random-dot)))
	      (list name (dot-x rd) (dot-y rd))))
	'(first second third))

;; ((FIRST 77 72) (SECOND 17 56) (THIRD 75 22))

(defmethod named-dot ((r random-dot) name)
  (list name (dot-x r) (dot-y r)))

(mapcar #'(lambda (name)
	    (named-dot (make-instance 'random-dot) name))
	'(first second third))




