# 1 — the extensible language
* In principle, anything you can do with a programming language you can do with a Turing machine; in practice, programming a Turing machine is not worth the trouble

* if you had to write some of the programs in this book in C, you might as well do it by writing a Lisp compiler in C first

* object-oriented programming is an extension of ideas already present in Lisp. It is one of the many abstractions that can be built on Lisp.

* The plan-and-implement method may have been a good way of building dams or launching invasions, but experience has not shown it to be as good a way of writing programs

* The big danger in plunging right into a project is the possibility that we will paint ourselves into a corner. If we had a more flexible language, could this worry be lessened? We do, and it is.

* It’s a long-standing principle of programming style that the functional elements of a program should not be too large.

* In typical code, once you abstract out the parts which are merely bookkeeping, what’s left is much shorter; the higher you build up the language, the less distance you will have to travel from the top down to it.

* Defining functions is comparatively straightforward. Macros provide a more general, but less well-understood, means of defining new operators
Over the past twenty years, the way people program has changed. Many of these changes—interactive environments, dynamic linking, even object-oriented programming—have been piecemeal attempts to give other languages some of the flexibility of Lisp

# 2 — functions
* Functions are objects in their own right. What defun really does is build one, and store it under the name given as the first argument. So as well as calling double, we can get hold of the function which implements it. The usual way to do so is by using the #’ (sharp-quote) operator. 

* a Common Lisp function is a first-class object, with all the same rights as more familiar objects like numbers and strings. So we can pass this function as an argument, return it, store it in a data structure, and so on:

* a language which allows functions as data objects must also provide some way of calling them. In Lisp, this function is apply. Generally, we call apply with two arguments: a function, and a list of arguments for it.

* because Common Lisp is lexically scoped, when we define a function containing free variables, the system must save copies of the bindings of those variables at the time the function was defined. Such a combination of a function and a set of variable bindings is called a closure

* closures are functions with local state. The simplest way to use this state
is in a situation like the following:
``` lisp
(let ((counter 0))
  (defun new-id () 
    (incf counter))
  (defun reset-id () 
    (setq counter 0)))
```

* Tail-recursion is desirable because many Common Lisp compilers can trans- form tail-recursive functions into loops. With such a compiler, you can have the elegance of recursion in your source code without the overhead of function calls at runtime.

* A function which isn’t tail-recursive can often be transformed into one that is
by embedding in it a local function which uses an accumulator. In this context, an accumulator is a parameter representing the value computed so far.

* In Common Lisp, functions are no longer made of lists—good implementations compile them into native machine code. But you can still write programs that write programs, because lists are the input to the compiler.

# 3 — functional programming
* Lisp and Lisp programs are both built out of a single raw material: the function. Like any building material, its qualities influence both the kinds of things we build, and the way we build them.

* Functional programming is a good idea in general. It is a particularly good idea in Lisp, because Lisp has evolved to support it.

* What skates are to ice, functional programming is to Lisp. Together the two allow you to travel more gracefully, with less effort. But if you are accustomed to another mode of travel, this may not be your experience at first.

* To write programs that are really indistinguishable from functional code, we
have to add one more condition. Functions can’t share objects with other code that doesn’t follow the rules.

* One problem with destructive operations is that, like global variables, they can destroy the locality of a program. When you’re writing functional code, you can narrow your focus: you only need consider the functions that call, or are called by, the one you’re writing.

* Lisp programmers did not adopt the functional style purely for aesthetic reasons. They use it because it makes their work easier.

# 4 — utility functions
* A piece of code can be called a utility if it seems too small to be considered as a separate application, and too general-purpose to be considered as part of a particular program.

* One of the unique characteristics of Lisp programming is the important role of functions as arguments. This is part of why Lisp is well-adapted to bottom-up programming. It’s easier to abstract out the bones of a function when you can pass the flesh back as a functional argument.

* Reading a bottom-up program requires one to understand all the new operators defined by the author. But this will nearly always be less work than having to understand all the code that would have been required without them.

# 5 — returning functions
* By defining functions to build and return new functions, we can magnify the effect of utilities which take functions as arguments.

* Scheme was the first Lisp to make functions lexical closures, and it is this which makes it interesting to have functions as return values.

* Being able to pass functions as arguments is a powerful tool for abstraction

* An orthogonal language is one in which you can express a lot by combining a small number of operators in a lot of different ways. Toy blocks are very orthogonal; a plastic model kit is hardly orthogonal at all

* As well as greater orthogonality, the ! operator brings a couple of other bene- fits. It makes programs clearer, because we can see immediately that (! #’foo) is the destructive equivalent of foo.

* If some function is expensive to compute, and we expect sometimes to make the same call more than once, then it pays to memoize: to cache the return values of all the previous calls, and each time the function is about to be called, to look first in the cache to see if the value is already known.

* With a memoized function, repeated calls are just hash-table lookups.

* Another common operation on functions is composition, denoted by the operator ◦. If f and g are functions, then f◦g is also a function, and f◦g(x) = f(g(x)). Closures also make it possible to define ◦ as a Lisp function.

* In general, composing and combining functions is more easily and efficiently done with macros. This is particularly true in Common Lisp, with its separate name-space for functions.



# 7 — macros

* In programming, the best way to learn is often to begin experimenting as soon as possible. A full theoretical understanding can come later.

# 11 — classic macros
* The let special form creates a new lexical environment; the expressions in the body of a let will be evaluated in an environment which may contain new variables.

# 14 — anaphoric macros
* it is possible to introduce a very limited form of anaphora into Lisp programs without causing ambiguity. An anaphor, it turns out, is a lot like a captured symbol. We can use anaphora in programs by designating certain symbols to serve as pronouns, and then writing macros intentionally to capture these symbols.

* The first and the last x in this expression [1] yield different values, because a setq intervenes. Admittedly, this is ugly code. The fact that it is even possible means that Lisp is not referentially transparent.

[1]
``` lisp
(list x
  (setq x (not x)) x)
  ```

# 15 — macros returning functions
* Lazy evaluation means only evaluating an expression when you need its value. One way to use lazy evaluation is to build an object known as a delay. A delay is a placeholder for the value of some expression. It represents a promise to deliver the value of the expression if it is needed at some later time.

# 20 — continuations
* A continuation is a program frozen in action: a single functional object containing the state of a computation. When the object is evaluated, the stored computation is restarted where it left off.

# 25 — object-oriented lisp
* In 1970, a multi-user computer system meant one or two big mainframes connected to a large number of dumb terminals. Now it is more likely to mean a large number of workstations connected to one another by a network. The processing power of the system is now distributed among individual users instead of centralized in one big computer.
Object-oriented programming breaks up traditional programs in much the same way: instead of having a single program which operates on an inert mass of data, the data itself is told how to behave, and the program is implicit in the interactions of these new data “objects.”

* Object-oriented programming, at a minimum, implies
  1. objects which have properties
   2. and respond to messages,
   3. and which inherit properties and methods from their parents.

* CLOS is a great deal larger and more powerful than our sketch, but not so large as to disguise its roots as an embedded language. Take defmethod as an example. Though CLTL2 does not mention it explicitly, CLOS methods have all the power of lexical closures. If we define several methods within the scope of some variable, then at runtime they will share access to the variable, just like closures. Methods can do this because, underneath the syntax, they are closures. In the expansion of a defmethod, its body appears intact in the body of a sharp-quoted lambda- expression.

* Extensibility is indeed one of the great benefits of the object-oriented style. Instead of being a single monolithic blob of code, a program is written in small pieces, each labelled with its purpose.

* just as it is inelegant to use a global variable where you could use a parameter, it could be inelegant to build a world of classes and instances when you could do the same thing with less effort in plain Lisp.

* It is no accident that we usually speak to CLOS through a layer of macros. Macros do transformation, and CLOS is essentially a program which takes programs built out of object-oriented abstractions, and translates them into programs built out of Lisp abstractions.

* With the addition of CLOS, Common Lisp has become the most powerful object-oriented language in widespread use. Ironically, it is also the language in which object-oriented programming is least necessary.
